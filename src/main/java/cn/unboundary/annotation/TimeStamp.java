package cn.unboundary.annotation;


import cn.unboundary.validation.TimeStampValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 时间校验注解
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TimeStampValidator.class)
public @interface TimeStamp {
    String message() default "请求已过时";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
