package cn.unboundary.controller;

import cn.unboundary.vo.result.RestRespone;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理拦截器抛出的异常，此类异常使用@RestControllerAdvice无法捕获
 */
@Slf4j
@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class GlobalErrorController extends AbstractErrorController {
    // 默认错误视图
    public static final String DEFAULT_ERROR_VIEW = "error";

    public GlobalErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @Override
    @Deprecated
    public String getErrorPath() {
        return null;
    }

    @RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response, String parameter) {
        ModelAndView modelAndView = new ModelAndView(DEFAULT_ERROR_VIEW);
        modelAndView.addObject(getErrorInfo(request));
        return modelAndView;
    }

    @RequestMapping
    public RestRespone error(HttpServletRequest request, String parameter) {
        return getErrorInfo(request);
    }

    private RestRespone getErrorInfo(HttpServletRequest request) {
        return RestRespone.error(Integer.valueOf(request.getAttribute("javax.servlet.error.status_code").toString()), request.getAttribute("javax.servlet.error.message").toString());
    }

}
