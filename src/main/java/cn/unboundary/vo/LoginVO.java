package cn.unboundary.vo;

import lombok.Data;

/**
 * 登录视图模型
 */
@Data
public class LoginVO {

    /**
     * 用户名
     */
    private String username;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 验证码Key
     */
    private String key;

}
