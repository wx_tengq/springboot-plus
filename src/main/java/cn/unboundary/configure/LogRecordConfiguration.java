package cn.unboundary.configure;

import cn.unboundary.service.UserService;
import com.mzt.logapi.beans.Operator;
import com.mzt.logapi.service.IOperatorGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;

/**
 * 操作日志配置类
 */
@Configuration
public class LogRecordConfiguration {
    @Autowired
    UserService userService;

    /**
     * 获取操作人
     * @return 获取操作人接口
     */
    @Bean
    public IOperatorGetService operatorGetService() {
        return () -> Optional.ofNullable(userService.getCurrentUser())
                .map(sysUser -> new Operator(sysUser.getUsername()))
                .orElseThrow(()->new IllegalArgumentException("user is null"));
    }
}

