package cn.unboundary.entity;

import cn.hutool.core.date.DatePattern;
import cn.unboundary.constant.RegexConstant;
import cn.unboundary.validation.ValidationGroup;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 用户实体
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUser extends BaseEntity  {
    private static final long serialVersionUID = 3533351213902459053L;

    /**
     * 用户名
     */
    @Pattern(regexp = RegexConstant.USERNAME, message = "用户名由大小写字母和数字组成，并且以字母开头，长度6～20")
    @NotBlank(message = "用户名不能为空", groups = ValidationGroup.Insert.class)
    private String username;

    /**
     * 登录密码
     */
    @Pattern(regexp = RegexConstant.PASSWORD, message = "密码由大小写字母和数字组成，并且以字母开头，长度8～16")
    @NotBlank(message = "密码不能为空", groups = ValidationGroup.Insert.class)
    private String password;

    /**
     * 姓名
     */
    @Pattern(regexp = RegexConstant.NICKNAME, message = "姓名由2～25个汉字组成")
    @NotBlank(message = "姓名不能为空", groups = ValidationGroup.Insert.class)
    private String nickname;

    /**
     * 所属部门ID
     */
    private Long orgId;

    /**
     * 是否可用（1：可用；0：不可用）
     */
    @Pattern(regexp = "^[01]$", message = "状态只能为0或1")
    private String enabled;

    /**
     * 简介
     */
    @Length(max = 64, message = "简介最长支持64个字符")
    private String intro;

    /**
     * 上一次登录时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date lastLoginTime;
}
