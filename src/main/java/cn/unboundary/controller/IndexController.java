package cn.unboundary.controller;

import cn.unboundary.constant.LogRecordType;
import com.mzt.logapi.starter.annotation.LogRecordAnnotation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 根接口
 */
@RestController
public class IndexController {
    /**
     * 服务器根路径
     * @return 响应内容
     */
    @GetMapping("/")
    @LogRecordAnnotation(
            fail = "访问主页失败",
            success = "访问主页成功",
            prefix = LogRecordType.ORDER, bizNo = "")
    public String index() {
        return "Service is running......";
    }
}
