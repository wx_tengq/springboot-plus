package cn.unboundary.constant;

public interface LogRecordType {
    String ORDER = "订单";
    String USER = "用户管理";
    String ROLE = "角色管理";
    String PERMS = "权限管理";
    String ORG = "组织管理";
}
