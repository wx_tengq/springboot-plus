package cn.unboundary.mapper;

import cn.unboundary.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色DAO
 */
public interface RoleMapper extends BaseMapper<Role> {
}
