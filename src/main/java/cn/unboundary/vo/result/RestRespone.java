package cn.unboundary.vo.result;

import cn.unboundary.configure.jackson.NullObjectToEmptyObjectSerializer;
import cn.unboundary.enums.ResultCode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 统一响应格式
 */
@Setter
@Getter
public class RestRespone implements Serializable {
    private static final long serialVersionUID = -168658541097740143L;
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 提示信息
     */
    private String msg;
    /**
     * 响应数据
     */
    @JsonSerialize(nullsUsing = NullObjectToEmptyObjectSerializer.class) // 当为nul时序列为{}
    private Object data;

    private RestRespone(ResultCode resultCode, Object data) {
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
        this.data = data;
    }

    private RestRespone(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static RestRespone ok() {
        return new RestRespone(ResultCode.SUCCESS, null);
    }

    public static RestRespone ok(String msg) {
        return new RestRespone(ResultCode.SUCCESS.getCode(), msg, null);
    }

    public static RestRespone ok(Object data) {
        return new RestRespone(ResultCode.SUCCESS, data);
    }

    public static RestRespone ok(String msg, Object data) {
        return new RestRespone(ResultCode.SUCCESS.getCode(), msg, data);
    }

    public static RestRespone error(String msg) {
        return new RestRespone(ResultCode.FAILED.getCode(), msg, null);
    }

    public static RestRespone error(Integer code, String msg) {
        return new RestRespone(code, msg, null);
    }

    public static RestRespone error(Integer code, String msg, Object data) {
        return new RestRespone(code, msg, data);
    }

    public static RestRespone error(ResultCode resultCode) {
        return new RestRespone(resultCode, null);
    }

    public static RestRespone error(ResultCode resultCode, Object data) {
        return new RestRespone(resultCode, data);
    }
}
