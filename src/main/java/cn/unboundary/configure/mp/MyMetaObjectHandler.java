package cn.unboundary.configure.mp;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

/**
 * 自定义填充公共字段
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入时自动填充create_time和update_time字段
     * @param metaObject metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        final DateTime date = DateUtil.date();
        setFieldValByName("createTime", date, metaObject);
        setFieldValByName("updateTime", date, metaObject);
    }

    /**
     * 更新时自动填充update_time字段
     * @param metaObject metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName("updateTime", DateUtil.date(), metaObject);
    }
}
