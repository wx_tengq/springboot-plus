package cn.unboundary.entity;

import cn.unboundary.constant.RegexConstant;
import cn.unboundary.validation.ValidationGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 角色实体
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Role extends BaseEntity {
    private static final long serialVersionUID = 8753714026675626300L;

    /**
     * 角色名称
     */
    @Pattern(regexp = RegexConstant.NAME, message = "角色名称由汉字、大小写字母和数字组成，长度2～16")
    @NotBlank(message = "角色名称不能为空", groups = ValidationGroup.Insert.class)
    private String name;

    /**
     * 角色编码
     */
    @Pattern(regexp = RegexConstant.CODE, message = "角色名称由大小写字母、数字和下划线组成，长度4～16")
    @NotBlank(message = "角色编码不能为空", groups = ValidationGroup.Insert.class)
    private String code;

    /**
     * 角色说明
     */
    @Length(max = 64, message = "角色说明最长支持64个字符")
    private String explain;
}
