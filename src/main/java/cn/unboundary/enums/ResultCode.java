package cn.unboundary.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Api状态码枚举
 */
@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(200, "请求成功！"),

    UNAUTHORIZED(401, "尚未认证，请先登录！"),
    REPEAT_LOGIN(402, "你已在另一台设备登录，本次登录已下线！"),
    FORBIDDEN(403, "权限不足！"),
    NOT_FOUND(404, "未找到该资源！"),
    REQUEST_METHOD_NOT_SUPPORT(405, "不支持此类型请求！"),
    REQUEST_PARAMETER_ERROR(1001, "请求参数有误"),
    REQUEST_PARAMETER_EMPTY(1002, "请求参数缺失"),
    REQUEST_PARAMETER_CASE_ERROR(1003, "请求参数转换出错"),
    HTTP_MEDIA_TYPE_NOT_SUPPORT(1004, "请求媒体类型不支持"),
    BODY_IS_MISSING(407, "缺少必要的请求体！"),
    BODY_NOT_MATCH(408, "请求体格式不正确！"),
    FAILED(424, "请求失败！"),
    INTERNAL_SERVER_ERROR(500, "服务器内部错误！"),
    SERVER_BUSY(503, "服务器正忙，请稍后再试！");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 提示消息
     */
    private final String msg;
}
