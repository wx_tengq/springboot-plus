package cn.unboundary.controller;

import cn.unboundary.constant.CaptchaConstant;
import cn.unboundary.constant.EndpointConstant;
import cn.unboundary.vo.CaptchaVO;
import cn.hutool.core.util.RandomUtil;
import com.wf.captcha.ArithmeticCaptcha;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 验证码接口
 */
@Validated
@RestController
public class CaptchaController {
    @Autowired
    private StringRedisTemplate redisTemplate;
    /**
     * 获取验证码
     */
    @GetMapping(EndpointConstant.CAPTCHA)
    public CaptchaVO captcha(@Range(min = 50, max = 300) @RequestParam(defaultValue = CaptchaConstant.DEFAULT_WIDTH) int width, @Range(min = 20, max = 120) @RequestParam(defaultValue = CaptchaConstant.DEFAULT_HEIGHT) int height) {
        // 获取图形验证码（算术类型）
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(width, height);
        // 设置运算位数
        captcha.setLen(CaptchaConstant.ARITHMETIC_DIGIT);
        // 获取图形验证码的Base64编码
        String image = captcha.toBase64();
        // 运算结果
        String code = captcha.text();
        // Key
        String key =  RandomUtil.randomString(16);
        // 存入redis并设置过期时间为15分钟
        redisTemplate.opsForValue().set(key, code, CaptchaConstant.EXPIRY_TIME, TimeUnit.MINUTES);
        return new CaptchaVO(image, key);
    }
}
