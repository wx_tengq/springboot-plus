package cn.unboundary.constant;

/**
 * 端点常量
 */
public interface EndpointConstant {

    /**
     * 端点
     */
    String INDEX = "/";

    /**
     * 所有端点
     */
    String ALL = "/**";

    /**
     * 图标
     */
    String ICON = "/favicon.ico";

    /**
     * 用户登录端点
     */
    String LOGIN = "/user/login";

    /**
     * 用户登出端点
     */
    String LOGOUT = "/user/logout";


    /**
     * 获取图形验证码端点
     */
    String CAPTCHA = "/captcha/get";

    /**
     * 日志端点
     */
    String PLUMELOG =  "/plumelog/**";

    /**
     * 监控端点
     */
    String MONITOR =  "/monitor/**";
    String ACTUATOR = "/actuator/**";

    /**
     * swagger文档端点
     */
    String DOC = "/doc.html";
    String V2 = "/v2/**";
    String V3 = "/v3/**";
    String WEBJARS = "/webjars/**";
    String SWAGGER_RESOURCES = "/swagger-resources/**";
    String SWAGGER_UI = "/swagger-ui.html";
}
