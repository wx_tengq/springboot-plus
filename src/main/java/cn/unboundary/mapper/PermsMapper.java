package cn.unboundary.mapper;

import cn.unboundary.entity.Perms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 权限DAO
 */
public interface PermsMapper extends BaseMapper<Perms> {
}
