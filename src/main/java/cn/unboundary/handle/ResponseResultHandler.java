package cn.unboundary.handle;

import cn.unboundary.annotation.IgnoreRestBody;
import cn.unboundary.vo.result.RestRespone;
import cn.unboundary.vo.result.IrregularVO;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Collection;

/**
 * 统一响应格式处理器
 */
@Configuration(proxyBeanMethods = false)
@RestControllerAdvice(basePackages = "cn.granularity.controller") // 限定拦截范围，对此路径下的controller进行拦截
public class ResponseResultHandler implements ResponseBodyAdvice<Object> {

    /**
     * 是否需要进行响应格式封装
     * @param methodParameter methodParameter
     * @param clazz clazz
     * @return 返回true不进行响应格式封装，返回false调用beforeBodyWrite()方法进行响应格式封装
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> clazz) {
        return !(methodParameter.hasMethodAnnotation(IgnoreRestBody.class) || methodParameter.getParameterType().isAssignableFrom(RestRespone.class));
    }

    /**
     * 响应格式封装结果
     * @param body 响应内容
     * @param methodParameter methodParameter
     * @param mediaType mediaType
     * @param clazz class
     * @param serverHttpRequest request
     * @param serverHttpResponse response
     * @return 封装后的响应内容
     */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> clazz, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (body == null) // 无响应内容
            return RestRespone.ok();
        else if (body instanceof String) // 响应内容为字符串
            return RestRespone.ok(body.toString());
        else if (body instanceof  Boolean || body instanceof Number || body instanceof Collection || body.getClass().isArray()) // 响应内容为布尔、数字、集合或者数组
            return RestRespone.ok(new IrregularVO<>(body));
        return RestRespone.ok(body);
    }
}
