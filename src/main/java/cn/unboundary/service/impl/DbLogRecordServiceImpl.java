package cn.unboundary.service.impl;

import cn.unboundary.mapper.LogRecordMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mzt.logapi.beans.LogRecord;
import com.mzt.logapi.service.ILogRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 存储操作日志到数据库
 */
@Service
public class DbLogRecordServiceImpl implements ILogRecordService {
    @Autowired
    LogRecordMapper logRecordMapper;

    /**
     * 保存日志
     * @param logRecord 待保存日志记录
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void record(LogRecord logRecord) {
        logRecordMapper.insert(logRecord);
    }

    @Override
    public List<LogRecord> queryLog(String bizKey) {
        QueryWrapper<LogRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("biz_key", bizKey);
        return logRecordMapper.selectList(wrapper);
    }

    @Override
    public List<LogRecord> queryLogByBizNo(String bizNo) {
        QueryWrapper<LogRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("biz_no", bizNo);
        return logRecordMapper.selectList(wrapper);
    }

}
