package cn.unboundary.configure.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * 布尔类型序列化器，将布尔值序列化为1或0.
 */
public class NumericBooleanSerializer extends JsonSerializer<Boolean> {
    public static final NumericBooleanSerializer instance = new NumericBooleanSerializer();

    @Override
    public void serialize(Boolean b, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeNumber(b ? 1 : 0);
    }
}
