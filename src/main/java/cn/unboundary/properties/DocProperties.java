package cn.unboundary.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("custom.doc")
public class DocProperties {
    /**
     * 项目名
     */
    private String title = "";
    /**
     * 项目简介
     */
    private String desc = "";
    /**
     * 版本
     */
    private String version = "";
    /**
     * 项目生产地址
     */
    private String serviceUrl = "";
    /**
     * 需要扫描的包
     */
    private String basePackage;
    /**
     * 作者
     */
    private String author = "";
    /**
     * 地址
     */
    private String url = "";
    /**
     * 联系邮箱
     */
    private String email = "";
    /**
     * 协议
     */
    private String license = "";
    /**
     * 协议地址
     */
    private String licenseUrl = "";
}
