package cn.unboundary.vo.result;

import lombok.Getter;

/**
 * 请求返回的数据为基本数据类型和数组（列表）时进行包装的类，避免破坏统一返回体的结构
 */
@Getter
public class IrregularVO<T>  {
    /**
     * 数据
     */
    private final T data;

    public IrregularVO() {
        data = null;
    }

    public IrregularVO(T data) {
        this.data = data;
    }
}

