package cn.unboundary.configure;

import cn.unboundary.properties.DocProperties;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 在线文档配置类
 */
@Configuration
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
@EnableConfigurationProperties(DocProperties.class)
@ConditionalOnProperty(value = "custom.doc.enable", havingValue = "true", matchIfMissing = true)
public class Knife4jConfigure {
    @Autowired
    DocProperties docProperties;

    /**
     * 注入Docket
     * @return Docket
     */
    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(groupApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(docProperties.getBasePackage()))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * Api基本信息设置
     * @return ApiInfo
     */
    private ApiInfo groupApiInfo() {
        Contact contact = new Contact(docProperties.getAuthor(), docProperties.getUrl(), docProperties.getEmail());
        return new ApiInfoBuilder()
                .title(docProperties.getTitle())
                .description(docProperties.getDesc())
                .termsOfServiceUrl(docProperties.getServiceUrl())
                .contact(contact)
                .license(docProperties.getLicense())
                .licenseUrl(docProperties.getLicenseUrl())
                .version(docProperties.getVersion())
                .build();

    }
}
