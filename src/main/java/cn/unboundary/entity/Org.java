package cn.unboundary.entity;

import cn.unboundary.constant.RegexConstant;
import cn.unboundary.validation.ValidationGroup;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 组织实体
 */
public class Org extends BaseEntity {
    private static final long serialVersionUID = -3943031037747893483L;

    /**
     * 顶级
     */
    public static final Long ORG_TOP_LEVEL = 0L;

    /**
     * 父级
     */
    private Long parentId;

    /**
     * 组织名称
     */
    @Pattern(regexp = RegexConstant.NAME, message = "组织名称由汉字、大小写字母和数字组成，长度2～16")
    @NotBlank(message = "组织名称不能为空", groups = ValidationGroup.Insert.class)
    private String label;

    /**
     * 组织说明
     */
    @Length(max = 64, message = "组织说明最长支持64个字符")
    private String explain;

    /**
     * 组织排序
     */
    @Range(min = 1, max = 9999, message = "组织排序范围1～9999")
    private Integer weight;

}
