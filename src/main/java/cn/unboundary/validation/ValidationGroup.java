package cn.unboundary.validation;

import javax.validation.groups.Default;

/**
 * 验证组
 */
public interface ValidationGroup {

    /**
     * 新增
     */
    interface Insert extends Default{}

    /**
     * 更新
     */
    interface Update extends Default{}
}
