package cn.unboundary.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

/**
 * 检索视图
 */
@Data
public class SearchVO {
    /**
     * 当前页
     */
    @Range(min = 1)
    private Integer current = 1;
    /**
     * 每页条目数
     */
    @Range(min = 1)
    private Integer size =  10;
    /**
     * 关键字
     */
    private String keyword = "";
    /**
     * 排序字段
     */
    private String field = "";
    /**
     * 排序规则
     */
    private String order = "";
}
