package cn.unboundary.annotation;

import java.lang.annotation.*;

/**
 * 统一响应结果忽略注解
 * 对于不想进行统一响应结果封装的接口添加此标识即可，在类上添加表示当前类中的接口均不进行封装，方法上添加表示此接口不进行统一封装
 */
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface IgnoreRestBody {
}
