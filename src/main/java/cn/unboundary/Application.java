package cn.unboundary;

import com.mzt.logapi.starter.annotation.EnableLogRecord;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动类
 * 日志(plumelog)访问地址：http://<ip>:<port>/plumelog/#/
 * 监控(monitor)访问地址：http://<ip>:<port>/monitor
 * 使用plumelog必须扫描com.plumelog包
 */
@EnableAsync
@SpringBootApplication
@EnableLogRecord(tenant = "cn.unboundary")
@MapperScan(basePackages = "cn.unboundary.mapper")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
