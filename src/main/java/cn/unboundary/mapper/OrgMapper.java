package cn.unboundary.mapper;

import cn.unboundary.entity.Org;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 组织DAO
 */
public interface OrgMapper extends BaseMapper<Org> {
}
