package cn.unboundary.entity;

import cn.unboundary.constant.RegexConstant;
import cn.unboundary.validation.ValidationGroup;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 权限实体
 */
public class Perms extends BaseEntity {
    private static final long serialVersionUID = -2230498688123593814L;

    /**
     * 顶级
     */
    public static final Long PERMS_TOP_LEVEL = 0L;

    /**
     * 菜单类型
     */
    public static final String  PERMS_TYPE_MENU = "0";

    /**
     * 按钮类型
     */
    public static final String PERMS_TYPE_BUTTON = "1";

    /**
     * 父级
     */
    private Long parentId;

    /**
     * 权限名称
     */
    @Pattern(regexp = RegexConstant.NAME, message = "权限名称由汉字、大小写字母和数字组成，长度2～16")
    @NotBlank(message = "权限名称不能为空", groups = ValidationGroup.Insert.class)
    private String label;

    /**
     * 编码
     */
    @Pattern(regexp = RegexConstant.CODE, message = "角色名称由大小写字母、数字和下划线组成，长度4～16")
    @NotBlank(message = "权限编码不能为空", groups = ValidationGroup.Insert.class)
    private String code;

    /**
     * 权限类型（0：菜单；1：按钮）
     */
    @Pattern(regexp = "^[01]$", message = "权限类型只能为0或1")
    @NotBlank(message = "权限类型不能为空", groups = ValidationGroup.Insert.class)
    private String type;

    /**
     * 路径（菜单必填）
     */
    @Pattern(regexp = "^[/a-zA-Z]{0,64}$")
    private String path;

    /**
     * 组件（菜单必填）
     */
    @Pattern(regexp = "^[/a-zA-Z]{0,64}$")
    private String component;

    /**
     * 菜单图标（菜单必填）
     */
    @Pattern(regexp = "^[a-zA-Z0-9-]{0,32}$")
    private String icon;

    /**
     * 排序（选填）
     */
    @Range(min = 1, max = 9999, message = "权限排序范围1～9999")
    private Integer weight;
}
