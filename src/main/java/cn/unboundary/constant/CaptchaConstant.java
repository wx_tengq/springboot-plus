package cn.unboundary.constant;

/**
 * 图形验证码常量
 */
public interface CaptchaConstant {
    /**
     * 默认宽度
     */
    String DEFAULT_WIDTH = "120";
    /**
     * 默认高度
     */
    String DEFAULT_HEIGHT = "40";
    /**
     * 算术位数
     */
    int ARITHMETIC_DIGIT = 2;
    /**
     * 过期时间（分钟）
     */
    int EXPIRY_TIME = 15;
}
