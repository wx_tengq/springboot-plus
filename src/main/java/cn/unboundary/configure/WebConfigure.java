package cn.unboundary.configure;

import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.hutool.core.date.DatePattern;
import cn.unboundary.constant.EndpointConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type.SERVLET;

/**
 * WebMvc配置类
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = SERVLET)
public class WebConfigure implements WebMvcConfigurer {

    /**
     * 注册日期时间格式化
     * @param registry 格式化注册器
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
        registrar.setTimeFormatter(DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN));
        registrar.setDateFormatter(DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN));
        registrar.setDateTimeFormatter(DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN));
        registrar.registerFormatters(registry);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 解决统一返回结果直接返回String报错问题，将StringHttpMessageConverter转换器去除掉
        converters.removeIf(httpMessageConverter -> httpMessageConverter.getClass() == StringHttpMessageConverter.class);
    }

    // 注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册Sa-Token的路由拦截器
        registry.addInterceptor(new SaRouteInterceptor((request, response, handler) -> {
            // 拦截
            System.out.println(request.getUrl());
        }))
        .addPathPatterns(EndpointConstant.ALL)
        .excludePathPatterns(EndpointConstant.ICON, EndpointConstant.INDEX, EndpointConstant.CAPTCHA, EndpointConstant.LOGIN,
                EndpointConstant.PLUMELOG, EndpointConstant.MONITOR, EndpointConstant.ACTUATOR,
                EndpointConstant.DOC, EndpointConstant.V2, EndpointConstant.V3, EndpointConstant.WEBJARS, EndpointConstant.SWAGGER_RESOURCES, EndpointConstant.SWAGGER_UI,
                "*.html", "*.css", "*.js", "*.png");
    }
}
