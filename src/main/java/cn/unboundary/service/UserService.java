package cn.unboundary.service;

import cn.unboundary.entity.SysUser;

/**
 * 用户服务接口
 */
public interface UserService {
    SysUser getCurrentUser();
}
