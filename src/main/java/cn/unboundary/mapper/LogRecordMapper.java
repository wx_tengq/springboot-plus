package cn.unboundary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mzt.logapi.beans.LogRecord;

/**
 * 操作日志DAO
 */
public interface LogRecordMapper extends BaseMapper<LogRecord> {
}
