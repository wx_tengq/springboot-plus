package cn.unboundary.mapper;

import cn.unboundary.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户DAO
 */
public interface UserMapper extends BaseMapper<SysUser> {
}
