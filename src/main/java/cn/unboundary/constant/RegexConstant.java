package cn.unboundary.constant;

/**
 * 正则表达式
 */
public interface RegexConstant {
    /**
     * 用户名
     */
    String USERNAME = "^[a-zA-Z][A-Za-z0-9]{6,19}$";
    /**
     * 密码
     */
    String PASSWORD = "^[a-zA-Z][A-Za-z0-9]{8,16}$";
    /**
     * 姓名
     */
    String NICKNAME = "^[\\u4E00-\\u9FA5]{2,25}$";

    /**
     * 名称
     */
    String NAME = "^[\\u4E00-\\u9FA5a-zA-Z0-9]{2,16}$";

    /**
     * 编码
     */
    String CODE = "^[a-zA-Z0-9_]{4,16}$";
}
