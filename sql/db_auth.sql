/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : db_auth

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 13/12/2021 23:04:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_log_record
-- ----------------------------
DROP TABLE IF EXISTS `t_log_record`;
CREATE TABLE `t_log_record` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `tenant` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户',
  `biz_key` varchar(200) COLLATE utf8mb4_general_ci NOT NULL COMMENT '业务前缀',
  `biz_no` varchar(200) COLLATE utf8mb4_general_ci NOT NULL COMMENT '业务ID',
  `operator` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作者(用户名)',
  `action` varchar(511) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作结果',
  `category` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作类别',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `detail` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作详情',
  PRIMARY KEY (`id`),
  KEY `t_log_record_index_operator` (`operator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='操作日志表';

-- ----------------------------
-- Records of t_log_record
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for t_org
-- ----------------------------
DROP TABLE IF EXISTS `t_org`;
CREATE TABLE `t_org` (
  `id` bigint NOT NULL COMMENT '主键',
  `parent_id` bigint NOT NULL DEFAULT '0' COMMENT '父级部门ID',
  `label` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
  `explain` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '无' COMMENT '备注',
  `weight` tinyint DEFAULT '1' COMMENT '排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_org_name_unique_index` (`label`) USING BTREE COMMENT '组织名称唯一索引',
  KEY `t_org_parent_id_index` (`parent_id`) USING BTREE COMMENT '父级组织普通索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of t_org
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for t_perms
-- ----------------------------
DROP TABLE IF EXISTS `t_perms`;
CREATE TABLE `t_perms` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单/按钮ID',
  `parent_id` bigint NOT NULL DEFAULT '0' COMMENT '上级菜单ID',
  `label` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单/按钮名称',
  `code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '权限标识',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型(0菜单；1按钮)',
  `path` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '对应路由path',
  `component` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '对应路由组件component',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `weight` tinyint NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_permission_name` (`label`) USING BTREE,
  UNIQUE KEY `t_permission_perms` (`code`) USING BTREE,
  KEY `t_permission_parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='权限表';

-- ----------------------------
-- Records of t_perms
-- ----------------------------
BEGIN;
INSERT INTO `t_perms` VALUES (1, 0, '用户管理', 'user-view', '0', '/user', 'views/user/index', 'el-icon-user-solid', 1, '2021-08-25 16:28:47', '2021-08-25 16:28:49');
INSERT INTO `t_perms` VALUES (2, 0, '角色管理', 'role-view', '0', '/role', 'views/role/index', 'el-icon-s-marketing', 2, '2021-08-25 16:29:16', '2021-08-25 16:29:19');
INSERT INTO `t_perms` VALUES (3, 0, '权限管理', 'permission-view', '0', '/permission', 'views/permission/index', 'el-icon-s-unfold', 3, '2021-08-25 16:29:47', '2021-08-25 16:29:49');
INSERT INTO `t_perms` VALUES (4, 0, '组织管理', 'org-view', '0', '/org', 'views/org/index', 'el-icon-s-flag', 4, '2021-08-25 16:30:26', '2021-08-25 16:30:30');
INSERT INTO `t_perms` VALUES (6, 0, '系统设置', 'system-view', '0', '', 'Layout', 'el-icon-s-tools', 5, '2021-08-25 22:35:29', '2021-08-25 22:35:31');
COMMIT;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `explain` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '无' COMMENT '说明',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_role_unique_index_name` (`name`),
  UNIQUE KEY `t_role_unique_index_code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
BEGIN;
INSERT INTO `t_role` VALUES (1, '超级管理员', 'super_admin', '无', '2021-08-25 15:25:28', '2021-08-25 15:25:31');
INSERT INTO `t_role` VALUES (2, '管理员', 'admin', '无', '2021-08-25 15:25:44', '2021-08-25 15:25:49');
COMMIT;

-- ----------------------------
-- Table structure for t_role_perms
-- ----------------------------
DROP TABLE IF EXISTS `t_role_perms`;
CREATE TABLE `t_role_perms` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `perms_id` bigint NOT NULL COMMENT '权限ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_role_perms_index_role_id` (`role_id`),
  KEY `t_role_perms_index_perms_id` (`perms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色权限表';

-- ----------------------------
-- Records of t_role_perms
-- ----------------------------
BEGIN;
INSERT INTO `t_role_perms` VALUES (1, 1, 1, '2021-08-25 16:30:53', '2021-08-25 16:30:56');
INSERT INTO `t_role_perms` VALUES (2, 1, 2, '2021-08-25 16:31:01', '2021-08-25 16:31:03');
INSERT INTO `t_role_perms` VALUES (3, 1, 3, '2021-08-25 16:31:07', '2021-08-25 16:31:09');
INSERT INTO `t_role_perms` VALUES (4, 1, 4, '2021-08-25 16:31:14', '2021-08-25 16:31:16');
INSERT INTO `t_role_perms` VALUES (5, 1, 5, '2021-08-28 15:20:49', '2021-08-28 15:20:52');
INSERT INTO `t_role_perms` VALUES (6, 1, 6, '2021-08-28 15:21:00', '2021-08-28 15:21:02');
INSERT INTO `t_role_perms` VALUES (7, 1, 7, '2021-08-28 15:21:12', '2021-08-28 15:21:14');
INSERT INTO `t_role_perms` VALUES (8, 1, 8, '2021-08-28 15:21:28', '2021-08-28 15:21:30');
INSERT INTO `t_role_perms` VALUES (9, 1, 9, '2021-08-28 15:21:39', '2021-08-28 15:21:41');
INSERT INTO `t_role_perms` VALUES (10, 1, 10, '2021-08-28 15:21:51', '2021-08-28 15:21:53');
INSERT INTO `t_role_perms` VALUES (11, 1, 11, '2021-08-28 15:27:01', '2021-08-28 15:27:03');
INSERT INTO `t_role_perms` VALUES (12, 1, 12, '2021-08-28 18:26:11', '2021-08-28 18:26:13');
INSERT INTO `t_role_perms` VALUES (13, 1, 13, '2021-08-28 18:26:21', '2021-08-28 18:26:23');
INSERT INTO `t_role_perms` VALUES (14, 1, 14, '2021-08-29 01:10:32', '2021-08-29 01:10:34');
INSERT INTO `t_role_perms` VALUES (15, 1, 15, '2021-08-29 02:00:29', '2021-08-29 02:00:31');
INSERT INTO `t_role_perms` VALUES (16, 1, 16, '2021-08-29 02:00:29', '2021-08-29 02:00:31');
INSERT INTO `t_role_perms` VALUES (17, 1, 20, '2021-09-07 22:54:14', '2021-09-07 22:54:16');
INSERT INTO `t_role_perms` VALUES (18, 1, 21, '2021-09-10 01:28:33', '2021-09-10 01:28:35');
COMMIT;

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  `id` bigint NOT NULL COMMENT '主键，自增',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名，最大长度：20字符',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码（加密后的密码），长度不能超过64个字符',
  `nickname` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名，长度不能超过20个字符',
  `org_id` bigint NOT NULL DEFAULT '-1' COMMENT '所属部门（部门编号），默认-1，代表无',
  `intro` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '无' COMMENT '简介',
  `enabled` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '账户是否可用（1：可用；0：不可用；默认1）',
  `last_login_time` datetime DEFAULT NULL COMMENT '上一次登录时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_sys_user_unique_index_username` (`username`) USING BTREE COMMENT '用户名唯一索引',
  KEY `t_user_org_id_index` (`org_id`) COMMENT '组织ID普通索引',
  KEY `t_sys_user_index_nickname` (`nickname`) COMMENT '姓名普通索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_user` VALUES (1, 'admin', '$2a$10$tZ4n0bvYi4iXVwj9DM.h2eUj2Al74GYwnVDUAVXxKrciAW1BJYOSe', '系统管理员', -1, '无', '1', '2021-09-02 09:25:25', '2021-08-24 13:54:08', '2021-08-31 12:37:42');
COMMIT;

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_user_role_index_user_id` (`user_id`),
  KEY `t_user_role_index_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户角色关联表';

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
BEGIN;
INSERT INTO `t_user_role` VALUES (1, 1, 1, '2021-08-31 12:36:46', '2021-08-31 12:36:46');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
