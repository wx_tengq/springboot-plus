package cn.unboundary.util;

import cn.unboundary.vo.result.RestRespone;
import cn.hutool.extra.spring.SpringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet工具类
 */
@Slf4j
public class ServletUtil {
    private static final ObjectMapper objectMapper = SpringUtil.getBean(ObjectMapper.class);

    /**
     * 获取request对象
     * @return HttpServletRequest
     */
    public static HttpServletRequest getRequest(){
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null)
            throw new RuntimeException("获取HttpServletRequest对象失败");
        return ((ServletRequestAttributes)requestAttributes).getRequest();
    }

    /**
     * 获取response对象
     * @return HttpServletResponse
     */
    public static HttpServletResponse getResponse(){
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null)
            throw new RuntimeException("获取HttpServletResponse对象失败");
        return ((ServletRequestAttributes)requestAttributes).getResponse();
    }

    public void write(RestRespone result) {
        try (PrintWriter writer = getResponse().getWriter()) {
            writer.write(objectMapper.writeValueAsString(result));
            writer.flush();
        } catch (IOException e) {
            log.error("写入响应结果出错====>", e);
            throw new RuntimeException("服务器出错");
        }
    }
}
