package cn.unboundary.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 验证码视图
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaptchaVO {
    /**
     * 验证码base64
     */
    private String image;
    /**
     * 验证码Key
     */
    private String key;
}
