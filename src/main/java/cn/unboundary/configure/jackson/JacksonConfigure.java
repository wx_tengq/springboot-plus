package cn.unboundary.configure.jackson;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Jackson配置类
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(ObjectMapper.class)
@AutoConfigureBefore(JacksonAutoConfiguration.class)
public class JacksonConfigure {
    @Autowired
    private JavaTimeModule javaTimeModule;
    @Bean
    @ConditionalOnMissingBean
    public Jackson2ObjectMapperBuilderCustomizer customizer() {
        return builder -> {
            // 设置地区
            builder.locale(Locale.CHINA);
            // 设置时区
            builder.timeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
            // 设置时间格式
            builder.simpleDateFormat(DatePattern.NORM_DATETIME_PATTERN);
            // 序列化时将Long类型序列化为字符串
            builder.serializerByType(Long.class, ToStringSerializer.instance);
            builder.modules(javaTimeModule);
        };
    }

}
