package cn.unboundary.exception;

import cn.unboundary.enums.ResultCode;
import lombok.Getter;

/**
 * 业务异常基类
 * 业务异常分为三种
 * <p>
 * 第一种是用户端操作的异常，例如用户输入参数为空，用户输入账号密码不正确
 * <p>
 * 第二种是当前系统业务逻辑出错，例如系统执行出错，磁盘空间不足
 * <p>
 * 第三种是第三方系统调用出错，例如文件服务调用失败，RPC调用超时
 */
@Getter
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 3307109158840092078L;
    /**
     * 状态码
     */
    private final Integer code;

    public ServiceException(String message) {
        super(message);
        this.code = ResultCode.FAILED.getCode();
    }

    public ServiceException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ServiceException(ResultCode resultCode) {
        super(resultCode.getMsg());
        this.code = resultCode.getCode();
    }
}
