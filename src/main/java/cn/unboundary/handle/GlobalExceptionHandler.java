package cn.unboundary.handle;

import cn.unboundary.enums.ResultCode;
import cn.unboundary.exception.ServiceException;
import cn.unboundary.vo.result.RestRespone;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局异常处理类（只能捕获请求到达Controller之后抛出的异常）
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    // 处理请求参数转换异常，一般为JSON解析异常
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public RestRespone handleHttpMessageNotReadableException() {
        return RestRespone.error(ResultCode.REQUEST_PARAMETER_CASE_ERROR);
    }

    // 处理不支持媒体类型异常
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public RestRespone handleHttpMediaTypeNotSupportedException() {
        return RestRespone.error(ResultCode.HTTP_MEDIA_TYPE_NOT_SUPPORT);
    }

    // 处理请求方法不被支持
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public RestRespone handleHttpRequestMethodNotSupportedException() {
        return RestRespone.error(ResultCode.REQUEST_METHOD_NOT_SUPPORT);
    }

    // 请求参数校验失败，拦截 @Valid 校验失败的情况
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RestRespone handleMethodArgumentNotValidException(BindingResult result) {
        return RestRespone.error(ResultCode.REQUEST_PARAMETER_ERROR.getCode(), result.getAllErrors().get(0).getDefaultMessage());
    }

    // 请求参数缺失异常
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public RestRespone handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        Map<String, String> map = new HashMap<>();
        map.put("缺失参数", e.getParameterName());
        map.put("参数类型", e.getParameterType());
        return RestRespone.error(ResultCode.REQUEST_PARAMETER_ERROR.getCode(), e.getMessage(), map);
    }

    // 处理数据绑定验证异常
    @ExceptionHandler(BindException.class)
    public RestRespone handleBindException(BindingResult result) {
        return RestRespone.error(ResultCode.REQUEST_PARAMETER_ERROR.getCode(), result.getAllErrors().get(0).getDefaultMessage());
    }

    // 处理用户业务异常
    @ExceptionHandler(ServiceException.class)
    protected Object handleUserException(ServiceException e) {
        return RestRespone.error(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(DataAccessException.class)
    protected Object handleDataAccessException(DataAccessException e) {
        log.error("数据库错误", e);
        return RestRespone.error(ResultCode.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MyBatisSystemException.class)
    protected Object handleMyBatisSystemException(MyBatisSystemException e) {
        log.error("MyBatis出错", e);
        return RestRespone.error(ResultCode.INTERNAL_SERVER_ERROR);
    }

    // 处理运行时系统异常（500错误码）
    @ExceptionHandler(RuntimeException.class)
    protected Object handleRuntimeException(RuntimeException e) {
        log.error(e.getMessage(), e);
        return RestRespone.error(ResultCode.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Throwable.class)
    protected Object handleThrowable(Throwable e) {
        log.error("未知运行时异常", e);
        return RestRespone.error(ResultCode.INTERNAL_SERVER_ERROR);
    }
}
